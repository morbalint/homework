package psql

import (
	"context"

	"github.com/uptrace/bun"
)

type User struct {
	//nolint:structcheck,gocritic,unused
	tableName struct{} `bun:"users"`
	ID        string   `bun:"id,notnull"`
	UserName  string   `bun:"user_name,notnull"`
	LastName  string   `bun:"last_name,notnull"`
	FirstName string   `bun:"first_name,notnull"`
	Password  string   `bun:"password,notnull"`
	Email     string   `bun:"email,notnull"`
	Mobile    string   `bun:"mobile,notnull"`
	ASZF      bool     `bun:"aszf,notnull"`
}

type UserPSQLRepository struct {
	db bun.IDB
}

func NewUserPSQLRepository(db *bun.DB) *UserPSQLRepository {
	return &UserPSQLRepository{db: db}
}

func (r *UserPSQLRepository) GetUserCount(
	ctx context.Context) (int, error) {
	user := new(User)
	count, err := r.db.NewSelect().
		Model(user).
		Count(ctx)
	if err != nil {
		return 0, err
	}
	return count, nil
}
