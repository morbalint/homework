package cmd

import (
	"context"
	"database/sql"
	"it-test/adapters/psql"
	"it-test/app"
	"it-test/app/query"
	"it-test/config"
	"it-test/pkg/server"
	"it-test/ports"

	"github.com/go-chi/chi/v5"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
	"github.com/uptrace/bun/driver/pgdriver"

	"net/http"

	"github.com/sirupsen/logrus"

	"github.com/spf13/cobra"
)

var (
	port string
)

var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "starts a http server",
	// Uncomment the following line if your bare application
	// has an action associated with it:
	Run: func(cmd *cobra.Command, args []string) {
		loader := configLoader()
		cfg := &config.AppConfig{}
		check(loader.LoadConfig(".", cfg))
		initLogLevel(cfg)
		initGocore(cfg)
		startHTTP(cmd.Context(), cfg)
	},
}

func init() {
	rootCmd.AddCommand(serverCmd)

	serverCmd.Flags().StringVarP(
		&port,
		"port",
		"p",
		"8080",
		"HTTP Server port to listen on.\nExample: it-test server -p 8000",
	)
}

func startHTTP(rootCtx context.Context, cfg *config.AppConfig) {
	logrus.WithContext(rootCtx).
		WithField("port", port).
		Info("Starting a http server")

	db := createPostgresConnection(cfg)
	defer func() {
		logrus.WithContext(rootCtx).Info("Closing db connections")
		err := db.Close()
		if err != nil {
			logrus.WithContext(rootCtx).WithError(err).Info("Failed to close db connections")
		} else {
			logrus.WithContext(rootCtx).Info("Closed db connections successfully")
		}
	}()

	deps := &Dependencies{
		DB: db,
	}

	application := newApplication(rootCtx, cfg, deps)
	server.RunHTTPServer(rootCtx, cfg, port, func(router chi.Router) http.Handler {
		return ports.HandlerFromMux(ports.NewHTTPServer(application), router)
	})
}

func newApplication(ctx context.Context, cfg *config.AppConfig, deps *Dependencies) *app.Application {
	logrus.WithContext(ctx).WithField("config", cfg).Info("Creating application")

	userRepository := psql.NewUserPSQLRepository(deps.DB)

	return &app.Application{
		Commands: &app.Commands{},
		Queries: &app.Queries{
			GetUserCount: query.NewGetUserCountHandler(userRepository),
		},
		AppConfig: cfg,
	}
}

func createPostgresConnection(cfg *config.AppConfig) *bun.DB {
	db := bun.NewDB(
		sql.OpenDB(pgdriver.NewConnector(
			pgdriver.WithDSN(cfg.DatabaseURL),
			pgdriver.WithApplicationName(cfg.ApplicationName),
		)),
		pgdialect.New(),
	)
	db.AddQueryHook(&DBLogger{})
	mustConnect(context.Background(), db)
	return db
}

func mustConnect(ctx context.Context, db *bun.DB) {
	logrus.Info(ctx, "Test db connection")
	var v string
	if err := db.NewRaw("select version()").Scan(ctx, &v); err != nil {
		logrus.Fatal(ctx, "Test db connection FAILED", err)
	}
	logrus.Info(ctx, "Test db connection OK", v)
}

type DBLogger struct{}

func (d DBLogger) BeforeQuery(ctx context.Context, _ *bun.QueryEvent) context.Context {
	return ctx
}

func (d DBLogger) AfterQuery(ctx context.Context, q *bun.QueryEvent) {
	logrus.Debug(ctx, q.Query, q.Result)
}
